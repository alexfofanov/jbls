FROM node:9.4.0-alpine

MAINTAINER Aleksandr Fofanov

RUN apk add --update sudo && \
    sudo npm install localtunnel -g --unsafe-perm && \
    rm -rf /var/cache/apk/*

CMD lt --local-host xidea.online --subdomain ${SUB_DOMAIN} --port 80
