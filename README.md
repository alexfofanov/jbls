# Obtain your own proxy tunnel for xidea.online


------------------
## Requirements

1. Docker (~17.12)


------------------
## How to get activated

1. Generate some random string. We will use this as subdomain on
[localtunnel.me](http://localtunnel.me)
```bash
export SUB_DOMAIN=`< /dev/urandom tr -dc a-z0-9 | head -c10; echo`
```

1. Clone this repo to some dir and `cd` to it

1. Build docker image
```bash
docker build . -t jbls:latest
```

1. Run container to obtain proxy tunnel url
```bash
docker run --name jbls -e SUB_DOMAIN=${SUB_DOMAIN} -d --restart always -t jbls
```

1. Find obtained url in container logs
```bash
docker logs jbls
```

1. Activate your IDE with obtained url.
